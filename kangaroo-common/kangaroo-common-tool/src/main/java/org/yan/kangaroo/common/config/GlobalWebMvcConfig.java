package org.yan.kangaroo.common.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.yan.kangaroo.common.interceptor.GlobalInterceptor;

/**
 * @author wangx
 * @date 12/02/2019 14:14
 */
@Configuration
@AllArgsConstructor
@Slf4j
public class GlobalWebMvcConfig implements WebMvcConfigurer {
    private final GlobalInterceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor);
    }
}
