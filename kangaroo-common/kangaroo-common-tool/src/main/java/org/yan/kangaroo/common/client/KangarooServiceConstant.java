package org.yan.kangaroo.common.client;

/**
 * mcp服务常量
 *
 * @author wangx
 * @date 11/14/2019 13:45
 */
public class KangarooServiceConstant {
    private KangarooServiceConstant() {
    }

    /**
     * 用户中心
     */
    public static final String KANGAROO_AUTH_SERVICE = "kangaroo-auth-service";

    /**
     * 邮件管理
     */
    public static final String KANGAROO_EMAIL = "kangaroo-email";
    /**
     * 文件存储
     */
    public static final String KANGAROO_FILE_STORAGE = "kangaroo-file-storage";
}
