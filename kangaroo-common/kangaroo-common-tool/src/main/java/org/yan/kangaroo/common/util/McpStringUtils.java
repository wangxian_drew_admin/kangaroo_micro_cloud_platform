package org.yan.kangaroo.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * 平台StringUtils
 *
 * @author wangx
 * @date 11/11/2019 15:00
 */
public class McpStringUtils {
    private McpStringUtils() {
    }

    public static String getUuid() {
        UUID uuid = UUID.randomUUID();
        return StringUtils.replace(uuid.toString(), "-", "").toUpperCase();
    }

    public static String getRequestId() {
        return UUID.randomUUID().toString().toUpperCase();
    }
}
