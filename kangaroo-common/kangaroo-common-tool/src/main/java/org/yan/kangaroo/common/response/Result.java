package org.yan.kangaroo.common.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.yan.kangaroo.common.threadlocal.GlobalRequestIdThreadLocal;

/**
 * API同意返回格式
 *
 * @author wangx
 * @date 11/08/2019 11:51
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Result<T> {
    private String code;

    private String message;

    private String requestId = GlobalRequestIdThreadLocal.getRequestId();

    private T data;

    private Result(T data) {
        this(BaseResponseCodeEnum.OK, data);
    }

    private Result(IResponseCode responseCode) {
        this(responseCode, null);
    }

    private Result(IResponseCode responseCode, T data) {
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
        this.data = data;
    }

    public static <T> Result<T> ok() {
        return ok(null);
    }


    public static <T> Result<T> ok(T data) {
        return new Result<>(data);
    }

    public static Result<?> error(IResponseCode responseCode) {
        return new Result<>(responseCode);
    }

    public static Result<?> fail(String code, String message) {
        Result<?> result = new Result<>();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }


}
