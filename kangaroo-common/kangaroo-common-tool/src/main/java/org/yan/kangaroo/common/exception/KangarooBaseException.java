package org.yan.kangaroo.common.exception;

import lombok.Data;
import org.yan.kangaroo.common.response.IResponseCode;

/**
 * @author wangx
 * @date 11/08/2019 23:47
 */
@Data
public class KangarooBaseException extends RuntimeException {
    private IResponseCode responseCode;

    public KangarooBaseException(IResponseCode responseCode) {
        super(responseCode.getMessage());
        this.responseCode = responseCode;
    }
}
