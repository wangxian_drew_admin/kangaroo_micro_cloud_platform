package org.yan.kangaroo.common.client.email;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.yan.kangaroo.common.client.KangarooServiceConstant;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.pojo.email.entity.EmailSendRecord;
import org.yan.kangaroo.pojo.email.vo.request.SendEmailRequest;

/**
 * 邮件服务feign api
 *
 * @author wangx
 * @date 11/13/2019 21:57
 */
@Primary
@FeignClient(value = KangarooServiceConstant.KANGAROO_EMAIL, path = "/internal/api/v1/emails")
public interface EmailApiService {
    /**
     * 发送邮件
     *
     * @param request request
     * @return ResultResponse
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Result<EmailSendRecord> sendEmail(@RequestBody SendEmailRequest request);
}
