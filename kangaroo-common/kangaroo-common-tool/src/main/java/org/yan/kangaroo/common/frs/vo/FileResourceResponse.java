package org.yan.kangaroo.common.frs.vo;

import lombok.Data;

/**
 * @author wangx
 * @date 11/08/2019 13:58
 */
@Data
public class FileResourceResponse {
    private String name;

}
