package org.yan.kangaroo.common.util.ip;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author wangx
 * @date 11/12/2019 16:41
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpAdInfo {
    private String nation;

    private String province;

    private String city;

    private String district;

    private String adcode;
}
