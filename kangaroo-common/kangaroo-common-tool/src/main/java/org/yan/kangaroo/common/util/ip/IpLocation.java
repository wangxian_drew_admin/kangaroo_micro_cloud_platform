package org.yan.kangaroo.common.util.ip;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author wangx
 * @date 11/12/2019 16:40
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpLocation {
    private String lat;

    private String lng;

}
