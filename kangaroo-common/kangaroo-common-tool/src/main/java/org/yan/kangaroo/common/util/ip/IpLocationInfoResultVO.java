package org.yan.kangaroo.common.util.ip;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author wangx
 * @date 11/12/2019 16:38
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpLocationInfoResultVO {
    private Integer status;

    private String message;

    private IpInfoResult result;
}
