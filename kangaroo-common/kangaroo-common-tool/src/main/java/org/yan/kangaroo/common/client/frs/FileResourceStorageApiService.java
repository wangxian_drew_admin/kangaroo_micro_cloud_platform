package org.yan.kangaroo.common.client.frs;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.yan.kangaroo.common.client.KangarooServiceConstant;
import org.yan.kangaroo.common.frs.vo.request.UploadFileBase64Request;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.pojo.storage.entity.FileResource;

/**
 * @author wangx
 * @date 11/14/2019 13:49
 */
@Primary
@FeignClient(value = KangarooServiceConstant.KANGAROO_FILE_STORAGE, path = "/internal/api/v1/files")
public interface FileResourceStorageApiService {


    /**
     * 上传文件,base64位编码
     *
     * @param base64Request 请求参数
     * @return ResultResponse
     */
    @PostMapping(value = "/upload/base64", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Result<FileResource> uploadBase64(@RequestBody UploadFileBase64Request base64Request);

    /**
     * 下载文件
     *
     * @param fileId 文件Id
     * @return 字节码
     */
    @GetMapping(value = "/preview/{fileId}")
    byte[] downFile(@PathVariable("fileId") Long fileId);

    /**
     * 获取文件信息
     *
     * @param fileId 文件id
     * @return ResultResponse
     */
    @GetMapping(value = "/{fileId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Result<FileResource> getInfo(@PathVariable("fileId") Long fileId);
}
