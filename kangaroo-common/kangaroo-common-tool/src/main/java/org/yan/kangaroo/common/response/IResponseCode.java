package org.yan.kangaroo.common.response;

/**
 * 返回代码
 *
 * @author wangx
 * @date 11/08/2019 11:55
 */
public interface IResponseCode {
    /**
     * 错误码
     *
     * @return code
     */
    String getCode();

    /**
     * 消息
     *
     * @return message
     */
    String getMessage();
}
