package org.yan.kangaroo.common.response;

/**
 * 一些基础的返回代码
 *
 * @author wangx
 * @date 11/08/2019 11:59
 */
public enum BaseResponseCodeEnum implements IResponseCode {
    /**
     * ok
     */
    OK("0", "操作成功"),

    ERROR("500", "操作失败,请稍后在试"),
    NOT_LOGGED_IN("500109", "该资源需要登录才能访问,请登录"),
    FORBIDDEN("403", "您无权限操作");

    private final String code;

    private final String message;

    BaseResponseCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
