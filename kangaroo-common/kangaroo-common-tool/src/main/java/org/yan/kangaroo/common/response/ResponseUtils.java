package org.yan.kangaroo.common.response;

import lombok.extern.slf4j.Slf4j;
import org.yan.kangaroo.common.util.JsonUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangx
 * @date 11/11/2019 15:29
 */
@Slf4j
public class ResponseUtils {
    private ResponseUtils() {
    }

    public static void outAsJson(HttpServletResponse response, Result<?> result) {
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getOutputStream().write(JsonUtils.getInstance().toJson(result).getBytes());
        } catch (IOException e) {
            log.error("[outAsJson] error:", e);
        }
    }
}
