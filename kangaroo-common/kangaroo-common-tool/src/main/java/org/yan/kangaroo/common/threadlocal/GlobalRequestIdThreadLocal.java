package org.yan.kangaroo.common.threadlocal;

/**
 * @author wangx
 * @date 12/02/2019 13:56
 */
public class GlobalRequestIdThreadLocal {
    private static final ThreadLocal<String> REQUEST_ID_HOLDER = new ThreadLocal<>();


    public static void setRequestId(String requestId) {
        REQUEST_ID_HOLDER.set(requestId);
    }

    public static String getRequestId() {
        return REQUEST_ID_HOLDER.get();
    }

    public static void clear() {
        REQUEST_ID_HOLDER.remove();
    }

}
