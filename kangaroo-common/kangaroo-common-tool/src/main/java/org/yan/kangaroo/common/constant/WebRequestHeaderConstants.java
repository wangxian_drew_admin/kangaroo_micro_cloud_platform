package org.yan.kangaroo.common.constant;

/**
 * @author wangx
 * @date 12/02/2019 14:03
 */
public class WebRequestHeaderConstants {
    private WebRequestHeaderConstants() {
    }

    /**
     * 请求ID
     */
    public static final String REQUEST_ID = "RequestId";
}
