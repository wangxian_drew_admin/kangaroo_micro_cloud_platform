package org.yan.kangaroo.common.response;

/**
 * 用于接收内部服务错误码
 *
 * @author wangx
 * @date 11/14/2019 18:09
 */
public class InternalServiceResponseCode implements IResponseCode {
    private String code;

    private String message;

    public InternalServiceResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
