package org.yan.kangaroo.common.util.ip;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author wangx
 * @date 11/12/2019 16:39
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpInfoResult {
    private String ip;

    private IpLocation location;

    @JsonProperty(value = "ad_info")
    private IpAdInfo adInfo;

}
