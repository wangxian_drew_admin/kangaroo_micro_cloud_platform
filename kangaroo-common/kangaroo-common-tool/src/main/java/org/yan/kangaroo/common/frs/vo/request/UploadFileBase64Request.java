package org.yan.kangaroo.common.frs.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.yan.kangaroo.pojo.storage.constant.FileStorageTypeEnum;

import javax.validation.constraints.NotBlank;

/**
 * 上传文件Base64编码
 *
 * @author wangx
 * @date 11/14/2019 10:23
 */
@Data
@ApiModel(value = "Base64编码格式上传文件")
public class UploadFileBase64Request {
    @NotBlank(message = "文件名不能为空")
    @ApiModelProperty("文件名字")
    private String fileName;

    @NotBlank(message = "文件类型不能为空")
    @ApiModelProperty("文件类型")
    private String contentType;

    @NotBlank(message = "文件内容base64编码不能为空")
    @ApiModelProperty(value = "文件内容base64内容")
    private String base64Content;


    @ApiModelProperty(value = "文件存储方式")
    private String storageType = FileStorageTypeEnum.ALIYUN.getType();
}
