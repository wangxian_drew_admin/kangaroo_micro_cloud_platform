package org.yan.kangaroo.common.response;


import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.yan.kangaroo.common.frs.vo.FileResourceResponse;
import org.yan.kangaroo.common.util.JsonUtils;

@RunWith(JUnit4.class)
@Slf4j
public class ResultResponseTest {
    @Test
    public void okTest() {
        FileResourceResponse fileResourceResponse = new FileResourceResponse();
        Result<FileResourceResponse> response = Result.ok(fileResourceResponse);
        String result = JsonUtils.getInstance().toJson(response);
        log.info("[okTest] response:{}", result);
        Result<FileResourceResponse> mockResponse = JsonUtils.getInstance().fromJson(result, new TypeReference<Result<FileResourceResponse>>() {
        });
        log.info("[okTest] mockResponse:{}", mockResponse);
    }

    @Test
    public void errorTest() {
    }
}
