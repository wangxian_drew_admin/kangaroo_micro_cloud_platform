package org.yan.kangaroo.common.response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.Year;

/**
 * description
 *
 * @author wangx 2020/07/10 12:01
 */
@RunWith(JUnit4.class)
public class DemoTest {

    @Test
    public void test1(){
        Year year = Year.now();
        Year lastYear = year.plusYears(-1);
        System.out.println(lastYear);
    }

}
