package org.yan.kangaroo.common.response;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

/**
 * @author wangx
 * @date 12/05/2019 13:55
 */
@RunWith(JUnit4.class)
@Slf4j
public class ArraySortTest {

    @Test
    public void arraySortTest() {
        int[] array = {89, 10, 46, 78, 9, 199};

        int length = array.length;
        for (int i = 0; i < length; i++) {
            int maxJ = length - i - 1;
            for (int j = 0; j < maxJ; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            System.out.println("maxJ:" + maxJ);
            System.out.println("第" + (i + 1) + "遍的结果:" + Arrays.toString(array));
        }
    }

    @Test
    public void jiujiuTest() {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " * " + i + " = " + i * j + "  ");
            }
            System.out.println();
        }
    }

    @Test
    public void treeMapTest() {
        TreeMap<Object, Object> treeMap = new TreeMap<>();
        treeMap.put("name", "drew.wang");
        treeMap.put("age", 16);
        treeMap.put("age1", 16);
        treeMap.put("a", "bb");
        treeMap.put("1", 1);
        System.out.println(treeMap);
    }

    @Test
    public void hashMapTest() {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put(null, "123");
        System.out.println(hashMap);
    }

    @Test
    public void hashSetTest() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("wang");
        System.out.println(hashSet);
    }



    @Test
    public void hashTableTest() {
        Hashtable<Object, Object> hashtable = new Hashtable<>();
        hashtable.put("1", 1);
        hashtable.put("3", 1);
        hashtable.put("2", 1);
        hashtable.put("5", 1);
        System.out.println(hashtable);
    }
}
