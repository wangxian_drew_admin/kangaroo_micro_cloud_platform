package org.yan.kangaroo.pojo.ucenter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;

/**
 * 用户登录历史
 *
 * @author wangx
 * @date 11/08/2019 15:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserLoginHistory extends BaseEntity {
    private Long accountId;

    /**
     * 登录账号类型
     */
    private String loginMethod;

    private String ip;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 操作系统
     */
    private String operatorSystem;

    /**
     * 国家
     */
    private String nation;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区域
     */
    private String district;
    /**
     * 行政区划代码
     */
    private Integer adcode;
    /**
     * 纬度
     */
    private String lat;
    /**
     * lng
     */
    private String lng;
    /**
     * 状态 0:失败 1:成功
     */
    private Boolean status;

}
