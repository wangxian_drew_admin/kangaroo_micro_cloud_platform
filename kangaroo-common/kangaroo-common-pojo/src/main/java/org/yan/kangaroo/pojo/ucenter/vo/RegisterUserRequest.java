package org.yan.kangaroo.pojo.ucenter.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangx
 * @date 11/08/2019 22:43
 */
@Data
public class RegisterUserRequest {
    @NotBlank(message = "请输入用户名")
    private String username;

    @NotBlank(message = "邮箱不能为空")
    private String email;

    @NotBlank(message = "密码不能为空")
    private String password;
}
