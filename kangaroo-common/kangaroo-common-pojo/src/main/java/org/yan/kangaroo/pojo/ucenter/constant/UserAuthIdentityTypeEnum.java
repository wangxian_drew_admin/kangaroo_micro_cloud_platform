package org.yan.kangaroo.pojo.ucenter.constant;

import lombok.Getter;

/**
 * 用户认证身份类型
 *
 * @author wangx
 * @date 11/09/2019 0:02
 */
@Getter
public enum UserAuthIdentityTypeEnum {
    /**
     * 用户名
     */
    USERNAME("username"),
    /**
     * 邮箱
     */
    EMAIl("email"),
    /**
     * 手机号
     */
    PHONE("phone"),
    /**
     * github
     */
    GITHUB("github");

    private String type;

    UserAuthIdentityTypeEnum(String type) {
        this.type = type;
    }

}
