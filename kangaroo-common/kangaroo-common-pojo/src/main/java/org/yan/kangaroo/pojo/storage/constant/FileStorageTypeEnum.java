package org.yan.kangaroo.pojo.storage.constant;

import lombok.Getter;

/**
 * @author wangx
 * @date 11/14/2019 10:06
 */
@Getter
public enum FileStorageTypeEnum {
    /**
     * 本地
     */
    LOCAL("local"),
    /**
     * 腾讯云
     */
    QCLOUD("qcloud"),

    /**
     * 阿里云
     */
    ALIYUN("aliyun");

    private final String type;

    FileStorageTypeEnum(String type) {
        this.type = type;
    }
}
