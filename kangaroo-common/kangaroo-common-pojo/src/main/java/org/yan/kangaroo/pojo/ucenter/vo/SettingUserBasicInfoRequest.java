package org.yan.kangaroo.pojo.ucenter.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 修改用户基本信息
 *
 * @author wangx
 * @date 11/14/2019 16:23
 */
@Data
public class SettingUserBasicInfoRequest {
    @NotBlank(message = "昵称不能为空")
    private String nickname;

    private Long accountId;
}
