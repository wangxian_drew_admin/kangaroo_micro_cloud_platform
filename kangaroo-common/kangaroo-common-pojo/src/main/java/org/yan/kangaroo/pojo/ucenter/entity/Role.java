package org.yan.kangaroo.pojo.ucenter.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;

/**
 * @author wangx
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "系统角色")
@Data
public class Role extends BaseEntity {


    /**
     * 角色ID(业务范畴)
     */
    @ApiModelProperty(value = "角色ID(业务范畴)")
    private Long roleId;

    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码")
    private String roleCode;

    /**
     * 角色名字
     */
    @ApiModelProperty(value = "角色名字")
    private String roleName;

    /**
     * 角色描述
     */
    @ApiModelProperty(value = "角色描述")
    private String description;
}

