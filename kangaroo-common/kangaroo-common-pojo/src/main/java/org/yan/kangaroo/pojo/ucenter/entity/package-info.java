/**
 * 授权模块,包含用户,角色,权限等信息
 *
 * @author wangx
 * @date 11/08/2019 15:43
 */
package org.yan.kangaroo.pojo.ucenter.entity;
