package org.yan.kangaroo.pojo.ucenter.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * @author wangx
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "微服务应用")
@Data
public class App extends BaseEntity {

    /**
     * app_id,业务主键
     */
    @ApiModelProperty(value = "app_id,业务主键")
    private Long appId;

    /**
     * 应用名称
     */
    @ApiModelProperty(value = "应用名称")
    @NotBlank(message = "app名称不能为空")
    private String appName;

    /**
     * 应用描述
     */
    @ApiModelProperty(value = "应用描述")
    @NotBlank(message = "app描述不能为空")
    private String appDescription;

}

