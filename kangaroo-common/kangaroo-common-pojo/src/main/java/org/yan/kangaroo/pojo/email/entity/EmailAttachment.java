package org.yan.kangaroo.pojo.email.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.yan.kangaroo.pojo.converter.LongToStringConverter;

import java.util.Date;

/**
 * 邮件附件
 *
 * @author wangx
 */
@Data
public class EmailAttachment {
    /**
     *
     */
    private Long id;

    /**
     * 邮件记录id
     */
    @JsonSerialize(using = LongToStringConverter.class)
    private Long emailRecordId;

    /**
     * 文件id
     */
    private Long fileId;

    /**
     * 文件名字
     */
    private String fileName;

    /**
     *
     */
    private Date createDate;

    /**
     *
     */
    private Date modifiedDate;
}

