package org.yan.kangaroo.pojo.email.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;
import org.yan.kangaroo.pojo.converter.LongToStringConverter;

/**
 * 邮件发送记录
 *
 * @author wangx
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EmailSendRecord extends BaseEntity {
    @JsonSerialize(using = LongToStringConverter.class)
    private Long emailRecordId;
    /**
     * 邮件主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

    /**
     * 收件人
     */
    private String to;

    /**
     * 抄送人
     */
    private String cc;

    /**
     * 是否有附件
     */
    private Boolean haveAttachment;

    /**
     * 发送者账户id
     */
    private Long sendAccountId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态:
     * ready:准备
     * sending:发送中
     * success:发送成功
     * fail: 发送失败
     */
    private String status;

    /**
     * 失败原因
     */
    private String failureReason;
}

