package org.yan.kangaroo.pojo.ucenter.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;
import org.yan.kangaroo.pojo.converter.LongToStringConverter;

/**
 * 用户基础信息
 *
 * @author wangx
 * @date 11/08/2019 15:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class User extends BaseEntity {
    /**
     * 账户id
     */
    @JsonSerialize(using = LongToStringConverter.class)
    private Long accountId;
    /**
     * 显示昵称
     */
    private String nickname;

    /**
     * 用户头像信息,关联文件存储id
     */
    private Long avatarId;

}
