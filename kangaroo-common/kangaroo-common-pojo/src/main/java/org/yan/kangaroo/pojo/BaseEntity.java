package org.yan.kangaroo.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.yan.kangaroo.pojo.converter.LongToStringConverter;

import java.time.LocalDateTime;

/**
 * @author wangx
 * @date 11/08/2019 15:08
 */
@Data
public class BaseEntity {
    @JsonSerialize(using = LongToStringConverter.class)
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime modifiedDate;


}
