package org.yan.kangaroo.pojo.email.constant;

import lombok.Getter;

/**
 * @author wangx
 * @date 11/13/2019 17:18
 */
@Getter
public enum EmailStatusEnum {
    /**
     * 准备
     */
    READY("ready"),
    /**
     * 发送中
     */
    SENDING("sending"),
    /**
     * 发送成功
     */
    SUCCESS("success"),
    /**
     * 发送失败
     */
    FAIL("fail");
    private String status;

    EmailStatusEnum(String status) {
        this.status = status;
    }
}
