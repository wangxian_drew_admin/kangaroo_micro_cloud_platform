package org.yan.kangaroo.pojo.storage.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;

@EqualsAndHashCode(callSuper = true)
@Data
public class FileResource extends BaseEntity {


    /**
     * 文件id,唯一,业务id
     */
    private Long fileId;

    /**
     * 文件名字
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String contentType;

    /**
     * 存储类型
     * local:本地
     * qcloud:腾讯云
     * qiniu:七牛云
     * aliyun:阿里云
     */
    private String storageType;

    /**
     * 存放关键字
     */
    private String keyword;

    /**
     * 大小
     */
    private Long size;
}

