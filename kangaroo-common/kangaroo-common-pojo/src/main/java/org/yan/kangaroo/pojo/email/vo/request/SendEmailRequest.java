package org.yan.kangaroo.pojo.email.vo.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author wangx
 * @date 11/13/2019 17:31
 */
@Data
public class SendEmailRequest {
    @NotBlank(message = "邮件主题不能为空")
    private String subject;

    @NotBlank(message = "邮件内容不能为空")
    private String content;

    @NotNull(message = "收件人不能为空")
    @Size(min = 1, max = 20, message = "收件人支持1到20个")
    private List<String> tos;

    private List<String> ccs;

    private List<Long> attachmentList;

    @NotNull(message = "发送人不能为空")
    private Long sendAccountId;

    private String remark;

}
