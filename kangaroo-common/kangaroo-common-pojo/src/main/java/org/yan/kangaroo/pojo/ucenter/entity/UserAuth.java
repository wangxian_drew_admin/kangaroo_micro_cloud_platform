package org.yan.kangaroo.pojo.ucenter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.yan.kangaroo.pojo.BaseEntity;

/**
 * 用户验证信息,用户支持多种方式登录
 *
 * @author wangx
 * @date 11/08/2019 15:46
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserAuth extends BaseEntity {
    /**
     * 关联用户信息
     */
    private Long accountId;
    /**
     * 身份类型,目前包含 username,email,phone,wechat,qq,github,weibo
     */
    private String identityType;
    /**
     * 标识信息,如用户名字
     */
    private String identifier;

    /**
     * 登录凭证,即密码
     */
    private String credential;
}
