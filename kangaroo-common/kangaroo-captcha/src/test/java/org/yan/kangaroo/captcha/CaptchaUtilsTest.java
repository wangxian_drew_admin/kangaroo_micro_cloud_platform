package org.yan.kangaroo.captcha;


import org.junit.Assert;

import java.awt.image.BufferedImage;

/**
 * 测试
 *
 * @author wangx 2020/05/22 22:28
 */
public class CaptchaUtilsTest {


    @org.junit.Test
    public void randomCode() {
        String verifyCode = CaptchaUtils.randomCode();
        Assert.assertNotNull(verifyCode);
        String verifyCode1 = CaptchaUtils.randomCode(4);
        Assert.assertNotNull(verifyCode1);
        Assert.assertEquals(4,verifyCode1.length());
    }

    @org.junit.Test
    public void generateCaptcha() {
        BufferedImage image = CaptchaUtils.generateCaptcha(CaptchaUtils.randomCode());
        Assert.assertNotNull(image);
    }
}
