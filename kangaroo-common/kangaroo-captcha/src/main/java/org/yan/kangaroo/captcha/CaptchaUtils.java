package org.yan.kangaroo.captcha;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 图形验证码
 *
 * @author xian 05/07/2019 19:11
 */
public class CaptchaUtils {
    /**
     * 验证码宽度
     */
    private static final int DEFAULT_WIDTH = 110;

    /**
     * 验证码高度
     */
    private static final int DEFAULT_HEIGHT = 38;

    /**
     * 验证码长度
     */
    private static final int CODE_LENGTH = 5;
    /**
     * 干扰线
     */
    private static final int INTERFERENCE_LINE = 10;

    private static final char[] CODE_SEQUENCE = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J',
            'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j',
            'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
            'x', 'y', 'z', '2', '3', '4', '5', '6', '7', '8', '9'};


    /**
     * Verify Code
     *
     * @return verify code
     */
    public static String randomCode() {
        Random random = new Random();
        StringBuilder verifyCode = new StringBuilder();
        for (int i = 0; i < CODE_LENGTH; i++) {
            verifyCode.append(CODE_SEQUENCE[random.nextInt(CODE_SEQUENCE.length)]);
        }
        return verifyCode.toString();
    }

    /**
     * Verify Code
     *
     * @param codeLength 验证码长度
     * @return verify code
     */
    public static String randomCode(int codeLength) {
        Random random = new Random();
        StringBuilder verifyCode = new StringBuilder();
        for (int i = 0; i < codeLength; i++) {
            verifyCode.append(CODE_SEQUENCE[random.nextInt(CODE_SEQUENCE.length)]);
        }
        return verifyCode.toString();
    }

    /**
     * 生成验证码
     *
     * @param verifyCode verify code content
     * @return image
     */
    public static BufferedImage generateCaptcha(String verifyCode) {
        Random random = new Random();
        BufferedImage image = new BufferedImage(DEFAULT_WIDTH, DEFAULT_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = image.createGraphics();

        graphics2D.setColor(Color.WHITE);
        graphics2D.fillRect(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        Font font = new Font("Fixedsys", Font.BOLD, 20);
        graphics2D.setFont(font);
        graphics2D.setColor(Color.BLACK);
        graphics2D.drawRect(0, 0, DEFAULT_WIDTH - 1, DEFAULT_HEIGHT - 1);

        graphics2D.setColor(Color.BLACK);
        //生成干扰线
        for (int i = 0; i < INTERFERENCE_LINE; i++) {
            int x = random.nextInt(DEFAULT_WIDTH);
            int y = random.nextInt(DEFAULT_HEIGHT);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            graphics2D.drawLine(x, y, x + xl, y + yl);
        }
        char[] codes = verifyCode.toCharArray();
        for (int i = 0; i < codes.length; i++) {
            char key = codes[i];
            int red = random.nextInt(255);
            int green = random.nextInt(255);
            int blue = random.nextInt(255);
            graphics2D.setColor(new Color(red, green, blue));
            graphics2D.drawString(key + "", (i + 1) * DEFAULT_WIDTH / 6, 26);
        }
        return image;
    }

}
