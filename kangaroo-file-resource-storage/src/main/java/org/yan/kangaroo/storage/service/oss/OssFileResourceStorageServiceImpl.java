package org.yan.kangaroo.storage.service.oss;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.yan.kangaroo.common.frs.vo.request.UploadFileBase64Request;
import org.yan.kangaroo.pojo.storage.constant.FileStorageTypeEnum;
import org.yan.kangaroo.pojo.storage.entity.FileResource;
import org.yan.kangaroo.storage.service.FileResourceService;
import org.yan.kangaroo.storage.service.IFileResourceStorageService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * 阿里云Oss存储
 *
 * @author wangx 2020/05/29 16:52
 */
@Service("ossFileStorageService")
@Slf4j
@AllArgsConstructor
public class OssFileResourceStorageServiceImpl implements IFileResourceStorageService {
    private final OssObjectResourceClient ossObjectResourceClient;
    private final FileResourceService fileResourceService;

    @Override
    public FileResource uploadObject(UploadFileBase64Request base64Request) {
        byte[] content = Base64.getDecoder().decode(base64Request.getBase64Content());
        String key = ossObjectResourceClient.uploadFile(new ByteArrayInputStream(content), base64Request.getFileName(), base64Request.getContentType());
        FileResource fileResource = new FileResource();
        fileResource.setContentType(base64Request.getContentType());
        fileResource.setFileName(base64Request.getFileName());
        fileResource.setSize((long) content.length);
        fileResource.setStorageType(FileStorageTypeEnum.ALIYUN.getType());
        fileResource.setKeyword(key);
        return fileResourceService.insert(fileResource);
    }

    @Override
    public FileResource uploadFile(MultipartFile file) {
        String key = null;
        try {
            key = ossObjectResourceClient.uploadFile(file.getInputStream(), file.getOriginalFilename(), file.getContentType());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        FileResource fileResource = new FileResource();
        fileResource.setContentType(file.getContentType());
        fileResource.setFileName(file.getOriginalFilename());
        fileResource.setSize(file.getSize());
        fileResource.setStorageType(FileStorageTypeEnum.ALIYUN.getType());
        fileResource.setKeyword(key);
        return fileResourceService.insert(fileResource);
    }

    @Override
    public InputStream getObject(String key) {
        return ossObjectResourceClient.getFile(key);
    }
}
