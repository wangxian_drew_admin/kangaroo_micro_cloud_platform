package org.yan.kangaroo.storage.service;

import org.springframework.web.multipart.MultipartFile;
import org.yan.kangaroo.common.frs.vo.request.UploadFileBase64Request;
import org.yan.kangaroo.pojo.storage.entity.FileResource;

import java.io.InputStream;

/**
 * @author wangx
 * @date 11/08/2019 10:57
 */
public interface IFileResourceStorageService {
    FileResource uploadObject(UploadFileBase64Request base64Request);

    FileResource uploadFile(MultipartFile file);

    InputStream getObject(String key);
}
