package org.yan.kangaroo.storage.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.yan.kangaroo.common.exception.KangarooBaseException;
import org.yan.kangaroo.common.frs.vo.request.UploadFileBase64Request;
import org.yan.kangaroo.pojo.storage.constant.FileStorageTypeEnum;
import org.yan.kangaroo.pojo.storage.entity.FileResource;
import org.yan.kangaroo.storage.constant.FileStorageResponseCodeEnum;
import org.yan.kangaroo.storage.service.cos.CosFileResourceStorageServiceImpl;
import org.yan.kangaroo.storage.service.oss.OssFileResourceStorageServiceImpl;

import java.io.InputStream;

/**
 * 文件存储工厂
 *
 * @author wangx 2020/05/29 16:55
 */
@Service
@AllArgsConstructor
public class FileStorageFactory {
    private final CosFileResourceStorageServiceImpl cosFileResourceStorageService;

    private final OssFileResourceStorageServiceImpl ossFileResourceStorageService;


    public FileResource uploadObject(UploadFileBase64Request base64Request) {
        if (FileStorageTypeEnum.ALIYUN.getType().equalsIgnoreCase(base64Request.getStorageType())) {
            return ossFileResourceStorageService.uploadObject(base64Request);
        } else if (FileStorageTypeEnum.QCLOUD.getType().equalsIgnoreCase(base64Request.getStorageType())) {
            return cosFileResourceStorageService.uploadObject(base64Request);
        } else {
            throw new KangarooBaseException(FileStorageResponseCodeEnum.STORAGE_TYPE_NOT_FOUND);
        }
    }

    public FileResource uploadFile(MultipartFile file, String storageType) {
        if (FileStorageTypeEnum.ALIYUN.getType().equalsIgnoreCase(storageType)) {
            return ossFileResourceStorageService.uploadFile(file);
        } else if (FileStorageTypeEnum.QCLOUD.getType().equalsIgnoreCase(storageType)) {
            return cosFileResourceStorageService.uploadFile(file);
        } else {
            throw new KangarooBaseException(FileStorageResponseCodeEnum.STORAGE_TYPE_NOT_FOUND);
        }
    }

    public InputStream getObject(String key, String storageType) {
        if (FileStorageTypeEnum.ALIYUN.getType().equalsIgnoreCase(storageType)) {
            return ossFileResourceStorageService.getObject(key);
        } else if (FileStorageTypeEnum.QCLOUD.getType().equalsIgnoreCase(storageType)) {
            return cosFileResourceStorageService.getObject(key);
        } else {
            throw new KangarooBaseException(FileStorageResponseCodeEnum.STORAGE_TYPE_NOT_FOUND);
        }
    }

}
