package org.yan.kangaroo.storage.constant;

import org.yan.kangaroo.common.response.IResponseCode;

/**
 * @author wangx
 * @date 11/14/2019 11:08
 */
public enum FileStorageResponseCodeEnum implements IResponseCode {
    /**
     * 文件资源未找到
     */
    FILE_RESOURCE_NOT_FOUND("700100", "文件资源未找到"),
    /**
     * 存储类型未定义
     */
    STORAGE_TYPE_NOT_FOUND("700101", "存储类型未定义");

    private final String code;

    private final String message;

    FileStorageResponseCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
