package org.yan.kangaroo.storage.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yan.kangaroo.common.response.Result;

/**
 * @author wangx
 * @date 11/21/2019 18:34
 */
@RestController
@Slf4j
public class HealthController {

    @RequestMapping("/health")
    public Result<String> health() {
        log.info("[health] begin check health");
        return Result.ok("up");
    }
}
