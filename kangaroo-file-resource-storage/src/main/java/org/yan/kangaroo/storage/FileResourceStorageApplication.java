package org.yan.kangaroo.storage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 文件资源存储服务
 *
 * @author wangx
 */
@SpringBootApplication(scanBasePackages = {"org.yan.kangaroo.common.**", "org.yan.kangaroo.storage.**"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"org.yan.kangaroo.common.client.**", "org.yan.kangaroo.common.config.**"})
@MapperScan("org.yan.kangaroo.storage.mapper")
public class FileResourceStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileResourceStorageApplication.class, args);
    }

}
