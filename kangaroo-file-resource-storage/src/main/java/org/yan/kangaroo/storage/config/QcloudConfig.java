package org.yan.kangaroo.storage.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * 腾讯云cos属性配置
 * @author wangx
 * @date 11/08/2019 10:53
 */
@Configuration
@ConfigurationProperties(prefix = "qcloud.cos")
@Data
public class QcloudConfig {
    private String secretId;

    private String secretKey;

    private String bucket;

    private String region;


}
