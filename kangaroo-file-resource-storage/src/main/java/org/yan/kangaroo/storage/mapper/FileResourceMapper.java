package org.yan.kangaroo.storage.mapper;

import org.apache.ibatis.annotations.Param;
import org.yan.kangaroo.pojo.storage.entity.FileResource;

/**
 * @author wangx
 * @date 11/14/2019 10:37
 */
public interface FileResourceMapper {
    /**
     * insert
     *
     * @param fileResource file
     */
    void insert(FileResource fileResource);

    /**
     * findb by file id
     *
     * @param fileId fileId
     * @return FileResource
     */
    FileResource findByFileId(@Param("fileId") Long fileId);
}
