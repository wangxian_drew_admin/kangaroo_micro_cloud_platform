package org.yan.kangaroo.storage.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云oss属性配置
 *
 * @author wangx 2020/05/29 11:42
 */
@Configuration
@ConfigurationProperties(prefix = "aliyun.oss")
@Data
public class AliyunOssConfig {

    private String endpoint;

    private String accessKeyId;

    private String accessKeySecret;

    private String bucket;

}
