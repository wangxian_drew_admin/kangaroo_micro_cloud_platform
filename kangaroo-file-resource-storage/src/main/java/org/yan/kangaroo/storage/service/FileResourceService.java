package org.yan.kangaroo.storage.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.yan.kangaroo.common.exception.KangarooBaseException;
import org.yan.kangaroo.common.util.IdWorkerUtils;
import org.yan.kangaroo.pojo.storage.entity.FileResource;
import org.yan.kangaroo.storage.constant.FileStorageResponseCodeEnum;
import org.yan.kangaroo.storage.mapper.FileResourceMapper;

import java.time.LocalDateTime;

/**
 * @author wangx
 * @date 11/14/2019 10:37
 */
@Service
@AllArgsConstructor
public class FileResourceService {

    private final FileResourceMapper fileResourceMapper;


    public FileResource insert(@NonNull FileResource fileResource) {
        fileResource.setFileId(IdWorkerUtils.getFlowIdWorkerInstance().nextId());
        fileResource.setCreateDate(LocalDateTime.now());
        fileResource.setModifiedDate(LocalDateTime.now());
        fileResourceMapper.insert(fileResource);
        return fileResource;
    }

    public FileResource findByFileId(@NonNull final Long fileId) {
        FileResource fileResource = fileResourceMapper.findByFileId(fileId);
        if (fileResource != null) {
            return fileResource;
        }
        throw new KangarooBaseException(FileStorageResponseCodeEnum.FILE_RESOURCE_NOT_FOUND);
    }
}
