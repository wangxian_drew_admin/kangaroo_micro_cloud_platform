package org.yan.kangaroo.storage.service.cos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("prod")
public class CosObjectResourceClientTest {
    @Autowired
    private CosObjectResourceClient cosObjectResourceClient;

    @Test
    public void uploadFile() {
        File file = new File("C:\\Users\\wangx\\Pictures\\Saved Pictures\\羊台山1.jpg");
        cosObjectResourceClient.uploadFile(file);
    }
}
