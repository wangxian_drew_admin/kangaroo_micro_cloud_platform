package org.yan.kangaroo.ucenter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.pojo.ucenter.entity.App;
import org.yan.kangaroo.ucenter.service.AppService;

/**
 * @author wangx 2020/6/4 17:12
 */
@RestController
@RequestMapping("/api/v1/apps")
@Api(tags = "APP(应用) API")
@Slf4j
public class AppController {
    private final AppService appService;

    public AppController(AppService appService) {
        this.appService = appService;
    }


    @PostMapping
    @ApiOperation("保存应用")
    public Result<?> insert(@RequestBody App app){
        appService.insert(app);
        return Result.ok();
    }
}
