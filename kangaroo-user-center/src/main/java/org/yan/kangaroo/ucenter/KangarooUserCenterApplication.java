package org.yan.kangaroo.ucenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author wangx
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("org.yan.kangaroo.ucenter.mapper")
public class KangarooUserCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(KangarooUserCenterApplication.class, args);
    }

}
