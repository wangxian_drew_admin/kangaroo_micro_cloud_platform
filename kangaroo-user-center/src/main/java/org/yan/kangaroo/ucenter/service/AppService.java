package org.yan.kangaroo.ucenter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.yan.kangaroo.common.util.IdWorkerUtils;
import org.yan.kangaroo.pojo.ucenter.entity.App;
import org.yan.kangaroo.ucenter.mapper.AppMapper;

import java.time.LocalDateTime;

/**
 * @author wangx 2020/6/4 17:08
 */
@Service
@Slf4j
public class AppService {
    private final AppMapper appMapper;


    public AppService(AppMapper appMapper) {
        this.appMapper = appMapper;
    }

    public void insert(App app) {
        app.setAppId(IdWorkerUtils.getFlowIdWorkerInstance().nextId());
        app.setCreateDate(LocalDateTime.now());
        app.setModifiedDate(LocalDateTime.now());
        int count = appMapper.insert(app);
        if (count < 1) {
            log.warn("[insert] 保存app失败");
        }

    }
}
