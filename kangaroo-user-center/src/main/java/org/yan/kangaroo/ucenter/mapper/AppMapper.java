package org.yan.kangaroo.ucenter.mapper;

import org.yan.kangaroo.pojo.ucenter.entity.App;

/**
 * @author wangx
 */
public interface AppMapper {
    /**
     * insert app
     *
     * @param app app
     * @return int
     */
    int insert(App app);
}
