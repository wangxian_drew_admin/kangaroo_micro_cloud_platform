package org.yan.kangaroo.email;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author wangx
 */
@SpringBootApplication(scanBasePackages = {"org.yan.kangaroo.common.**", "org.yan.kangaroo.email.**"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"org.yan.kangaroo.common.client.**", "org.yan.kangaroo.common.config.**"})
@MapperScan("org.yan.kangaroo.email.mapper")
public class EmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailApplication.class, args);
    }

}
