package org.yan.kangaroo.email.service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yan.kangaroo.common.exception.KangarooBaseException;
import org.yan.kangaroo.email.constant.McpEmailResponseCodeEnum;
import org.yan.kangaroo.email.mapper.EmailSendRecordMapper;
import org.yan.kangaroo.pojo.email.entity.EmailSendRecord;

import java.time.LocalDateTime;

/**
 * @author wangx
 * @date 11/13/2019 17:30
 */
@Service
@Slf4j
public class EmailSendRecordService {
    @Autowired
    private EmailSendRecordMapper emailSendRecordMapper;

    public EmailSendRecord insert(@NonNull EmailSendRecord sendRecord) {
        sendRecord.setCreateDate(LocalDateTime.now());
        sendRecord.setModifiedDate(LocalDateTime.now());
        emailSendRecordMapper.insert(sendRecord);
        return sendRecord;
    }

    public void update(@NonNull EmailSendRecord sendRecord) {
        sendRecord.setModifiedDate(LocalDateTime.now());
        emailSendRecordMapper.updateStatus(sendRecord);
    }

    public EmailSendRecord findByRecordId(Long recordId) {
        EmailSendRecord sendRecord = emailSendRecordMapper.findByRecordId(recordId);
        if (sendRecord != null) {
            return sendRecord;
        }
        throw new KangarooBaseException(McpEmailResponseCodeEnum.EMAIL_RECORD_NOT_FOUND);
    }
}
