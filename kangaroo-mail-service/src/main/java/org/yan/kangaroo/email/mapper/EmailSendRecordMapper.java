package org.yan.kangaroo.email.mapper;

import org.yan.kangaroo.pojo.email.entity.EmailSendRecord;

/**
 * @author wangx
 * @date 11/13/2019 17:17
 */
public interface EmailSendRecordMapper {
    /**
     * 插入记录
     *
     * @param sendRecord 记录
     */
    void insert(EmailSendRecord sendRecord);

    /**
     * 修改状态
     *
     * @param sendRecord 记录
     */
    void updateStatus(EmailSendRecord sendRecord);

    EmailSendRecord findByRecordId(Long recordId);
}
