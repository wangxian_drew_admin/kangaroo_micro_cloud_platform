package org.yan.kangaroo.email.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author xian
 * @date 06/04/2019
 */

@Configuration
@ConfigurationProperties(prefix = "email.sender")
@Getter
@Setter
public class EmailConfig {
    private String email;

    private String name;

    private String password;

    private String serverAddress;

    private String protocol;

    private Integer port;


}
