package org.yan.kangaroo.email.constant;

import org.yan.kangaroo.common.response.IResponseCode;

/**
 * @author wangx
 * @date 11/13/2019 18:02
 */
public enum McpEmailResponseCodeEnum implements IResponseCode {
    /**
     * 邮件发送失败
     */
    SEND_FAIL("600100", "邮件发送失败"),
    EMAIL_RECORD_NOT_FOUND("600101", "邮件发送记录未找到");

    private String code;

    private String message;

    McpEmailResponseCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
