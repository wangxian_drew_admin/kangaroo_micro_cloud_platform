package org.yan.kangaroo.email.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.email.service.EmailSendRecordService;
import org.yan.kangaroo.email.service.EmailSendService;
import org.yan.kangaroo.pojo.email.entity.EmailSendRecord;
import org.yan.kangaroo.pojo.email.vo.request.SendEmailRequest;

/**
 * @author wangx
 * @date 11/13/2019 17:36
 */
@RestController
@RequestMapping("/internal/api/v1/emails")
@Slf4j
public class EmailController {
    @Autowired
    private EmailSendService emailSendService;

    @Autowired
    private EmailSendRecordService sendRecordService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<EmailSendRecord> sendEmail(@RequestBody @Validated SendEmailRequest request) {
        log.info("[sendEmail] begin request params:{}", request);
        EmailSendRecord record = emailSendService.sendEmail(request);
        log.info("[sendEmail] end result:{}", record);
        return Result.ok(record);
    }

    @GetMapping(value = "/info")
    public Result<EmailSendRecord> getEmailInfo(@RequestParam("recordId") Long recordId) {
        log.info("[getEmailInfo] begin recordId:{}", recordId);
        EmailSendRecord sendRecord = sendRecordService.findByRecordId(recordId);
        log.info("[getEmailInfo] end result:{}", sendRecord);
        return Result.ok(sendRecord);
    }
}
