package org.yan.kangaroo.auth.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.yan.kangaroo.auth.mapper.UserAuthMapper;
import org.yan.kangaroo.pojo.ucenter.entity.UserAuth;

/**
 * 用户验证
 *
 * @author wangx
 * @date 11/08/2019 23:31
 */
@Service
@AllArgsConstructor
public class UserAuthService {
    private final UserAuthMapper userAuthMapper;

    public UserAuth findByIdentifier(String identifier) {
        return userAuthMapper.findByIdentifier(identifier);
    }

    public UserAuth findByIdentifierAndIdentityType(String identifier, String identityType) {
        return userAuthMapper.findByIdentifierAndIdentityType(identifier, identityType);
    }


    public void insert(UserAuth userAuth) {
        userAuthMapper.insert(userAuth);
    }


}
