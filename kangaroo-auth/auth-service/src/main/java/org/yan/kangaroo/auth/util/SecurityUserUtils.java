package org.yan.kangaroo.auth.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.yan.kangaroo.auth.config.security.SecurityUserDetails;

/**
 * @author wangx
 * @date 11/14/2019 13:59
 */
@Slf4j
public class SecurityUserUtils {
    public static SecurityUserDetails getCurrentUser() {
        SecurityUserDetails userDetails = (SecurityUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("[getCurrentUser] userAccountId:{}", userDetails.getAccountId());
        return userDetails;
    }
}
