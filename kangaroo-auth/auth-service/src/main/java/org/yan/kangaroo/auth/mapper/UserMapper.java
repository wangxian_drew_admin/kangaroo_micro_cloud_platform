package org.yan.kangaroo.auth.mapper;

import org.apache.ibatis.annotations.Param;
import org.yan.kangaroo.pojo.ucenter.entity.User;
import org.yan.kangaroo.pojo.ucenter.vo.SettingUserBasicInfoRequest;

import java.util.List;

/**
 * @author wangx
 * @date 11/08/2019 23:40
 */
public interface UserMapper {
    /**
     * 根据账户id获取用户
     * @param accountId 账户id
     * @return User
     */
    User findByAccountId(@Param("accountId") Long accountId);

    /**
     * 新增用户
     * @param user user
     */
    void insert(User user);

    /**
     * 查询所有用户
     * @return all User
     */
    List<User> listAll();

    /**
     * 根据用户头像
     * @param accountId 用户账户id
     * @param fileId 头像资源id
     */
    void updateAvatar(@Param("accountId") Long accountId, @Param("fileId") Long fileId);

    /**
     * 更新用户基本信息
     * @param basicInfoRequest 用户基本信息
     */
    void updateBasicInfo(SettingUserBasicInfoRequest basicInfoRequest);
}
