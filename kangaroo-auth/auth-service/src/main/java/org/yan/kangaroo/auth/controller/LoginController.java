package org.yan.kangaroo.auth.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yan.kangaroo.common.response.BaseResponseCodeEnum;
import org.yan.kangaroo.common.response.Result;

/**
 * @author wangx
 * @date 11/11/2019 18:03
 */
@RestController
@Slf4j
public class LoginController {
    @RequestMapping(value = "/auth-login", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> login() {
        log.info("[login] 需要登录");
        return Result.error(BaseResponseCodeEnum.NOT_LOGGED_IN);
    }
}
