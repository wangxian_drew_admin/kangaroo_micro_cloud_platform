package org.yan.kangaroo.auth.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.yan.kangaroo.auth.config.security.SecurityUserDetails;
import org.yan.kangaroo.auth.constant.response.ResponseCodeEnum;
import org.yan.kangaroo.auth.util.SecurityUserUtils;
import org.yan.kangaroo.common.client.frs.FileResourceStorageApiService;
import org.yan.kangaroo.common.exception.KangarooBaseException;
import org.yan.kangaroo.common.frs.vo.request.UploadFileBase64Request;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.common.util.FileUtils;
import org.yan.kangaroo.common.util.JsonUtils;
import org.yan.kangaroo.pojo.ucenter.entity.User;
import org.yan.kangaroo.pojo.ucenter.vo.SettingUserBasicInfoRequest;
import org.yan.kangaroo.pojo.storage.entity.FileResource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * 用户信息设置业务处理
 *
 * @author wangx
 * @date 11/14/2019 15:12
 */
@Service
@AllArgsConstructor
@Slf4j
public class UserInfoSettingService {
    private final UserService userService;

    private final FileResourceStorageApiService fileResourceStorageApiService;


    public void settingAvatar(MultipartFile file) {
        UploadFileBase64Request fileBase64Request = new UploadFileBase64Request();
        try {
            fileBase64Request.setBase64Content(Base64.getEncoder().encodeToString(file.getBytes()));
            fileBase64Request.setContentType(file.getContentType());
            fileBase64Request.setFileName(file.getOriginalFilename());
            log.debug("[settingAvatar] update avatar request params:{}", JsonUtils.getInstance().toJson(fileBase64Request));
            Result<FileResource> fileResponse = fileResourceStorageApiService.uploadBase64(fileBase64Request);
            log.info("[settingAvatar] response:{}", fileResponse);
            SecurityUserDetails userDetails = SecurityUserUtils.getCurrentUser();
            userService.updateAvatar(userDetails.getAccountId(), fileResponse.getData().getFileId());
        } catch (IOException e) {
            throw new KangarooBaseException(ResponseCodeEnum.USER_AVATAR_SETTING_FAIL);
        }
    }

    public void previewAvatar(HttpServletResponse response) throws IOException {
        User user = userService.findByAccountId(SecurityUserUtils.getCurrentUser().getAccountId());
        if (user.getAvatarId().equals(0L)) {
            InputStream inputStream = getClass().getResourceAsStream("/templates/default_avatar.jpg");
            FileUtils.preview(response, inputStream);
        } else {
            byte[] bytes = fileResourceStorageApiService.downFile(user.getAvatarId());
            FileUtils.preview(response, bytes);
        }
    }

    public void updateBasicInfo(SettingUserBasicInfoRequest basicInfoRequest) {
        userService.updateBasicInfo(basicInfoRequest);
    }
}
