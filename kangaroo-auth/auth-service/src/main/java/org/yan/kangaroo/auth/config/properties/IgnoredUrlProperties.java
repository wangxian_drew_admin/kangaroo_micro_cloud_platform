package org.yan.kangaroo.auth.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 过滤掉的url
 *
 * @author wangx
 * @date 11/08/2019 22:55
 */

@Configuration
@ConfigurationProperties(prefix = "mcp.auth-ignored")
@Data
public class IgnoredUrlProperties {
    private List<String> urls;
}
