package org.yan.kangaroo.auth.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.yan.kangaroo.auth.mapper.UserLoginHistoryMapper;
import org.yan.kangaroo.pojo.ucenter.entity.UserLoginHistory;

import java.time.LocalDateTime;

/**
 * @author wangx
 * @date 11/12/2019 17:13
 */
@Service
@AllArgsConstructor
@Slf4j
public class UserLoginHistoryService {
    private final UserLoginHistoryMapper loginHistoryMapper;

    public void insert(@NonNull UserLoginHistory userLoginHistory) {
        userLoginHistory.setCreateDate(LocalDateTime.now());
        userLoginHistory.setModifiedDate(LocalDateTime.now());
        loginHistoryMapper.insert(userLoginHistory);
    }

    public void localLoginInsert(@NonNull UserLoginHistory userLoginHistory) {
        userLoginHistory.setCreateDate(LocalDateTime.now());
        userLoginHistory.setModifiedDate(LocalDateTime.now());
        loginHistoryMapper.localIpInsert(userLoginHistory);
    }
}
