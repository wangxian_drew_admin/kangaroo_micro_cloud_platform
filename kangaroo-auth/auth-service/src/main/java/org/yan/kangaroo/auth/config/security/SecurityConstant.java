package org.yan.kangaroo.auth.config.security;

/**
 * 安全配置常量
 *
 * @author wangx
 * @date 11/11/2019 15:18
 */
public class SecurityConstant {
    private SecurityConstant() {

    }

    /**
     * 登录路径
     */
    public static final String LOGIN_URL = "/login";

    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * token加密可以
     */
    public static final String JWT_SIGN_KEY = "mcp_auth_P@ssw89d";


    /**
     * token头名称
     */
    public static final String TOKEN_HEADER = "Authorization";

    /**
     * token有效期一周
     */
    public static final long TOKEN_EFFECTIVE_TIME = 1000 * 60 * 60 * 24 * 7;
    /**
     * token里面包含的用户信息
     */
    public static final String TOKEN_USER_INFO = "userInfo";


}
