package org.yan.kangaroo.auth.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yan.kangaroo.common.response.Result;

/**
 * @author wangx
 * @date 11/19/2019 16:06
 */
@RestController
@RequestMapping("/api/v1/configs")
@RefreshScope
@Slf4j
public class ConfigCenterController {

    @Value("${wuxing.name:}")
    private String name;

    @GetMapping("/name")
    public Result<String> getWuXingName() {
        log.info("[getWuXingName] 验证配置中心");
        return Result.ok(name);
    }
}
