package org.yan.kangaroo.auth.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yan.kangaroo.auth.constant.response.ResponseCodeEnum;
import org.yan.kangaroo.auth.mapper.UserMapper;
import org.yan.kangaroo.auth.util.SecurityUserUtils;
import org.yan.kangaroo.common.exception.KangarooBaseException;
import org.yan.kangaroo.common.util.IdWorkerUtils;
import org.yan.kangaroo.pojo.ucenter.constant.UserAuthIdentityTypeEnum;
import org.yan.kangaroo.pojo.ucenter.entity.User;
import org.yan.kangaroo.pojo.ucenter.entity.UserAuth;
import org.yan.kangaroo.pojo.ucenter.vo.RegisterUserRequest;
import org.yan.kangaroo.pojo.ucenter.vo.SettingUserBasicInfoRequest;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author wangx
 * @date 11/08/2019 23:41
 */
@Service
@AllArgsConstructor
@Slf4j
public class UserService {
    private final UserMapper userMapper;

    private final UserAuthService userAuthService;


    public User findByAccountId(Long accountId) {
        User user = userMapper.findByAccountId(accountId);
        if (user == null) {
            throw new KangarooBaseException(ResponseCodeEnum.ACCOUNT_NOT_FOUND);
        }
        return user;
    }

    public User addUser() {
        User user = new User();
        user.setAccountId(IdWorkerUtils.getFlowIdWorkerInstance().nextId());
        user.setNickname("Kangaroo" + user.getAccountId());
        user.setCreateDate(LocalDateTime.now());
        user.setModifiedDate(LocalDateTime.now());
        userMapper.insert(user);
        return user;
    }

    @Transactional(rollbackFor = Exception.class)
    public void registerUser(RegisterUserRequest userRequest) {
        UserAuth usernameAuth = userAuthService.findByIdentifier(userRequest.getUsername());
        if (usernameAuth != null) {
            throw new KangarooBaseException(ResponseCodeEnum.USERNAME_HAS_BEEN_USED);
        }
        UserAuth emailAuth = userAuthService.findByIdentifier(userRequest.getEmail());
        if (emailAuth != null) {
            throw new KangarooBaseException(ResponseCodeEnum.EMAIL_HAS_BEEN_USED);
        }
        User userInfo = addUser();
        String credential = new BCryptPasswordEncoder().encode(userRequest.getPassword());
        usernameAuth = new UserAuth();
        usernameAuth.setAccountId(userInfo.getAccountId());
        usernameAuth.setIdentityType(UserAuthIdentityTypeEnum.USERNAME.getType());
        usernameAuth.setIdentifier(userRequest.getUsername());
        usernameAuth.setCredential(credential);
        usernameAuth.setCreateDate(LocalDateTime.now());
        usernameAuth.setModifiedDate(LocalDateTime.now());
        userAuthService.insert(usernameAuth);
        emailAuth = new UserAuth();
        BeanUtils.copyProperties(usernameAuth, emailAuth);
        emailAuth.setIdentityType(UserAuthIdentityTypeEnum.EMAIl.getType());
        emailAuth.setIdentifier(userRequest.getEmail());
        userAuthService.insert(emailAuth);
    }

    public List<User> listAll() {
        return userMapper.listAll();
    }

    public void updateAvatar(Long accountId, Long fileId) {
        userMapper.updateAvatar(accountId, fileId);
    }

    public void updateBasicInfo(SettingUserBasicInfoRequest basicInfoRequest) {
        basicInfoRequest.setAccountId(SecurityUserUtils.getCurrentUser().getAccountId());
        userMapper.updateBasicInfo(basicInfoRequest);

    }
}
