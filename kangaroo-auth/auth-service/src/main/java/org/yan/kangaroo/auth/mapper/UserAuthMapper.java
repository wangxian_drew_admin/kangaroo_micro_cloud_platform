package org.yan.kangaroo.auth.mapper;

import org.apache.ibatis.annotations.Param;
import org.yan.kangaroo.pojo.ucenter.entity.UserAuth;

/**
 * @author wangx
 * @date 11/08/2019 23:34
 */
public interface UserAuthMapper {
    /**
     * 根据用户认证标识查询
     * @param identifier 用户认证标识
     * @return User认证对象
     */
    UserAuth findByIdentifier(@Param("identifier") String identifier);

    /**
     * 新增用户认证
     * @param userAuth 用户认证
     */
    void insert(UserAuth userAuth);

    /**
     * 根据用户认证标识以及认证方式查询
     * @param identifier 用户认证标识
     * @param identityType 认证方式
     * @return 用户认证对象
     */
    UserAuth findByIdentifierAndIdentityType(@Param("identifier") String identifier, @Param("identityType") String identityType);
}
