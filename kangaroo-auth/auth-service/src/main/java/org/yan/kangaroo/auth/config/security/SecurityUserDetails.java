package org.yan.kangaroo.auth.config.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.yan.kangaroo.pojo.ucenter.entity.User;
import org.yan.kangaroo.pojo.converter.LongToStringConverter;

import java.util.Collection;

/**
 * 实现Spring Security UserDetails
 *
 * @author wangx
 * @date 11/08/2019 23:19
 */
@Slf4j
@Data
@NoArgsConstructor
public class SecurityUserDetails implements UserDetails {
    @JsonSerialize(using = LongToStringConverter.class)
    private Long accountId;

    private String username;

    private String password;


    public SecurityUserDetails(User user, String username, String password) {
        this.username = username;
        this.password = password;
        this.accountId = user.getAccountId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
