package org.yan.kangaroo.auth.constant.response;

import org.yan.kangaroo.common.response.IResponseCode;

/**
 * @author wangx
 * @date 11/08/2019 23:43
 */
public enum ResponseCodeEnum implements IResponseCode {
    /**
     * 账户未找到
     */
    ACCOUNT_NOT_FOUND("500100", "账户未找到"),
    USERNAME_HAS_BEEN_USED("500101", "用户名已被使用"),
    EMAIL_HAS_BEEN_USED("500102", "邮箱已被使用"),
    CAPTCHA_PARAMS_DEFECT("500103", "请传入图形验证码所需参数captchaId或code"),
    VERIFY_CODE_INVALID("500104", "验证码已过期，请重新获取"),
    VERIFY_CODE_INPUT_ERROR("500105", "验证码输入错误"),
    USERNAME_OR_PASSWORD_ERROR("500106", "用户名或密码错误"),
    LOGIN_FAILED("500107", "登录已失效,请重新登录"),
    AUTHENTICATION_FAILED("500108", "身份认证失败,请重新登录"),
    EMAIL_OR_VERIFY_CODE_ID_PARAMS_DEFECT("500110", "邮箱或验证码参数缺失"),
    USER_AVATAR_SETTING_FAIL("500111", "头像设置失败"),
    LOGIN_VERIFY_CODE_SEND_FAIL("500112", "登录验证码发送失败");

    private final String code;

    private final String message;

    ResponseCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
