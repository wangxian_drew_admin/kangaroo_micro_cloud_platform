package org.yan.kangaroo.auth.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.yan.kangaroo.auth.service.UserInfoSettingService;
import org.yan.kangaroo.auth.service.UserService;
import org.yan.kangaroo.auth.util.SecurityUserUtils;
import org.yan.kangaroo.common.response.Result;
import org.yan.kangaroo.pojo.ucenter.entity.User;
import org.yan.kangaroo.pojo.ucenter.vo.RegisterUserRequest;
import org.yan.kangaroo.pojo.ucenter.vo.SettingUserBasicInfoRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangx
 * @date 11/08/2019 22:47
 */
@RestController
@RequestMapping("/api/v1/users")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    private final UserInfoSettingService userInfoSettingService;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> register(@RequestBody @Validated RegisterUserRequest userRequest) {
        userService.registerUser(userRequest);
        return Result.ok();
    }

    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> getCurrentUser() {
        log.info("[getCurrentUser] begin");
        UserDetails userDetails = SecurityUserUtils.getCurrentUser();
        log.info("[getCurrentUser] end current user:{}", userDetails);
        return Result.ok(userDetails);
    }

    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<User> getUserInfo() {
        return Result.ok(userService.findByAccountId(SecurityUserUtils.getCurrentUser().getAccountId()));
    }

    @PutMapping(value = "/setting/basic", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> updateBasicInfo(@RequestBody @Validated SettingUserBasicInfoRequest basicInfoRequest) {
        log.info("[updateBasicInfo] begin request params:{}", basicInfoRequest);
        userInfoSettingService.updateBasicInfo(basicInfoRequest);
        log.info("[updateBasicInfo] end");
        return Result.ok();

    }

    @PostMapping(value = "/setting/avatar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> settingAvatar(@RequestPart("file") MultipartFile avatarFile) {
        log.info("[settingAvatar] begin fileName:{},fileSize:{}b", avatarFile.getOriginalFilename(), avatarFile.getSize());
        userInfoSettingService.settingAvatar(avatarFile);
        return Result.ok();
    }

    @GetMapping(value = "/preview/avatar")
    public void previewAvatar(HttpServletResponse response) throws IOException {
        userInfoSettingService.previewAvatar(response);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<?> listAllUser() {
        return Result.ok(userService.listAll());
    }
}
