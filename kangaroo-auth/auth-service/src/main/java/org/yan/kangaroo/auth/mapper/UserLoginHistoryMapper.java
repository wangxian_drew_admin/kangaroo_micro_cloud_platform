package org.yan.kangaroo.auth.mapper;

import org.yan.kangaroo.pojo.ucenter.entity.UserLoginHistory;

/**
 * @author wangx
 * @date 11/12/2019 16:56
 */
public interface UserLoginHistoryMapper {
    /**
     * 新增用户登录记录
     * @param loginHistory 登录信息
     */
    void insert(UserLoginHistory loginHistory);

    /**
     *  本地调式登录记录新增
     * @param loginHistory 登录信息
     */
    void localIpInsert(UserLoginHistory loginHistory);
}
