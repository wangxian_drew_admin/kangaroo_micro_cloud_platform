package org.yan.kangaroo.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 用户权限认证
 *
 * @author wangx
 */
@SpringBootApplication(scanBasePackages = {"org.yan.kangaroo.common.**", "org.yan.kangaroo.auth.**", "org.yan.kangaroo.pojo.**"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"org.yan.kangaroo.common.client.**", "org.yan.kangaroo.common.config.**"})
@MapperScan("org.yan.kangaroo.auth.mapper")
public class AuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }

}
